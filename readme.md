# Welcome to Cameroon


En : Welcome to Cameroon is a website which gives the time for my country Cameroon. This is a basic Symfony project.

Fr : Welcome to Cameroon est un site internet qui donne l'heure de mon pays le Cameroun. C'est un projet Symfony de base.

## Development environment / Environnement de développement

### Pre-requirement / Pré-requis

* PHP 7.4
* Composer
* Symfony CLI

En : You can check the pre-requirement (except Docker and Docker-compose) with the following command (from the Symfony CLI):

Fr : Vous pouvez vérifier la pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony):

```bash
symfony book:check-requirements
symfony check:requirements
```

### Launch the working environment / Lancer l'environnement de développement

```bash
symfony serve -d
```